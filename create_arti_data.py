import csv
import random
district_name=[]
with open('data/csv/dists.csv','r') as temp:
    reader = csv.DictReader(temp)
    for row in reader:
        district_name.append(row['dist'])

crop = ['wheat','mango','rice','coconut']
disease=['test1','test2','test3']
with open('data/csv/dist_crop.csv','w+') as temp:
    field_name = ['district','major_crop','major_disease','crops','disease']
    writer = csv.DictWriter(temp,fieldnames=field_name)
    writer.writeheader()
    for dist in district_name:
        temp_crop=random.choice(crop)
        temp_disease = random.choice(disease)
        writer.writerow({'district':dist,'major_crop':temp_crop,'major_disease':temp_disease,'crops':",".join(crop),'disease':",".join(disease)})

    